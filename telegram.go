package telegram

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"
)

func SendText(msg string) error {
	if err := godotenv.Load(); err != nil {
		return fmt.Errorf("Error cargando archivo .env: %v", err)
	}

	token := os.Getenv("TELEGRAM_BOT_TOKEN")
	if token == "" {
		return fmt.Errorf("TELEGRAM_BOT_TOKEN no encontrado en el archivo .env")
	}
	chatId := os.Getenv("TELEGRAM_CHANNEL_ID")
	if chatId == "" {
		return fmt.Errorf("TELEGRAM_CHANNEL_ID no encontrado en el archivo .env")
	}

	url := fmt.Sprintf("https://api.telegram.org/bot%s/sendMessage", token)

	body := fmt.Sprintf(`{
		"text": "%s",
		"parse_mode": "HTML",
		"chat_id": "%s"
	}`, msg, chatId)

	resp, err := http.Post(url, "application/json", bytes.NewBuffer([]byte(body)))
	if err != nil {
		return fmt.Errorf("Error enviando solicitud: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return fmt.Errorf("Error response. Status Code: %d", resp.StatusCode)
	}

	log.Println("Response status:", resp.Status)

	return nil
}
